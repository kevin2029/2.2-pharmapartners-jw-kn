const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const ExaminationSchema = require("./schemas/examination.schema");

const PhysicalExaminationScheme = new Schema({
    patient: {
        type: Schema.Types.ObjectId,
        ref: "patient",
        required: [true, "patient is nodig."],
    },
    lengte: [ExaminationSchema],
    gewicht: [ExaminationSchema],
    gewichtBijQI27: [ExaminationSchema],
    queteletIndexBMI: [ExaminationSchema],
    systolischeBloeddruk: [ExaminationSchema],
    diastolischeBloeddruk: [ExaminationSchema],
    tienJaarSterfteRisico: [ExaminationSchema],
    streefwaardeBloeddrukDiastolisch: [ExaminationSchema],
    streefwaardeBloeddrukSystolisch: [ExaminationSchema],
    gewichtBijQI25: [ExaminationSchema],
});

module.exports = getmodel("PhysicalExamination", PhysicalExaminationScheme);