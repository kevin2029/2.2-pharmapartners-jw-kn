const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const CurrentProblemsScheme = require("./schemas/currentProblems.schema");
const EpisodeScheme = require("./schemas/episode.schema");
const HistoryScheme = require("./schemas/history.schema");

const VisitcardScheme = new Schema({
    patient: {
        type: Schema.Types.ObjectId,
        ref: "patient",
        required: [true, "patient is nodig."],
    },
    medication: [{
            medicationTitle: {
                type: Schema.Types.ObjectId,
                ref: "medicationInformation",
            },
            addition: {
                type: String,
            },
            concentrations: {
                type: String,
            },
            duration: {
                type: String,
                required: [true, "Duratie nodig."],
            },
    }, ],

    journal: [EpisodeScheme],

    currentProblems: [CurrentProblemsScheme],

    history: [HistoryScheme]
});

module.exports = getmodel("Visitcard", VisitcardScheme);