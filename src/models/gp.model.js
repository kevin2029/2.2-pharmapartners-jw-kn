const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const AddressSchema = require("./schemas/address.schema");
const NameSchema = require("./schemas/name.schema");

const GPSchema = new Schema({
    fullName: {
        type: NameSchema,
    },
    fullAddress: {
        type: AddressSchema,
    },
    birthDate: {
        type: Date,
        required: [true, "Geboortedatum nodig."],
    },
    bsn: {
        type: String,
        required: [true, "BSN nodig."],
    },
    phoneNumber: {
        type: String,
        required: [true, "Telefoon nummer nodig."],
    },
    gender: {
        type: String,
        enum: ["M", "V", "A"],
    },
    email: {
        type: String,
    },
    username: {
        type: String,
        required: [true, "Username is nodig"],
    },
    password: {
        type: String,
        required: [true, "Password is nodig"],
    },
    tempSecret: {
        type: String,
    },
});

module.exports = getmodel("gp", GPSchema);