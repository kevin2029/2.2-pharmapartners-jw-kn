const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");

const MedicationInformationSchema = new Schema({
    title: {
        type: String,
        required: [true, "Title nodig."],
    },
});

module.exports = getmodel("MedicationInformation", MedicationInformationSchema);