const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const AddressSchema = require("./schemas/address.schema");

const VisitSchema = new Schema({
    patient: {
        type: Schema.Types.ObjectId,
        ref: "patient",
    },
    generalPractitioner: {
        type: Schema.Types.ObjectId,
        ref: "generalPractitioner",
    },
    visitCard: {
        type: Schema.Types.ObjectId,
        ref: "visitCard",
    },
    destinationAddress: AddressSchema,

    date: {
        type: Date,
        required: [true, "Datum nodig."],
    },
});

module.exports = getmodel("Visit", VisitSchema);