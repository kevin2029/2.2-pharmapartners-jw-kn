const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");

const LoggingSchema = new Schema({
    message: {
        type: String,
        required: [true, "Geef de logging op!"],
    },
    dateCreated: {
        type: String,
        required: [true],
    },
});

module.exports = getmodel("Logging", LoggingSchema);