const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const NameSchema = new Schema({
    firstName: {
        type: String,
        required: [true, "Voornaam nodig."],
    },
    middleName: {
        type: String,
    },
    lastName: {
        type: String,
        required: [true, "Achternaam nodig."],
    },
});

module.exports = NameSchema;