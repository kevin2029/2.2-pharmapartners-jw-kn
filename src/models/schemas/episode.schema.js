const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EpisodeScheme = new Schema({
    date: {
        type: Date,
        required: [true, "Datum nodig."],
    },
    s: {
        type: String
    },
    o: {
        type: String
    },
    e: {
        type: String
    },
    p: {
        type: String
    },
});

module.exports = EpisodeScheme;
