const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ExaminationSchema = new Schema({
    value: {
        type: String,
        required: [true, "vul waarde in"],
    },
    unit: {
        type: String,
        required: [true, "vul eenheid in"],
    },
});

module.exports = ExaminationSchema;