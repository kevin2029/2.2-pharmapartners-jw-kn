const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CurrentProblemsScheme = new Schema({
    date: {
        type: Date,
        required: [true, "Datum nodig."],
    },
    description: {
        type: String,
        required: [true, "Beschrijving nodig"]
    },
    code: {
        type: String,
        required: [true, "code nodig"]
    },
});

module.exports = CurrentProblemsScheme;