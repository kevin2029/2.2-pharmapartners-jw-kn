const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AddressSchema = new Schema({
	street: {
		type: String,
		required: [true, 'Straat nodig.']
	},
	houseNumber: {
		type: Number,
		required: [true, 'Huisnummer nodig.']
	},
	city: {
		type: String,
		required: [true, 'Plaats nodig.']
	},
	postalCode: {
		type: String,
		required: [true, 'Postcode nodig.']
	}
});


module.exports = AddressSchema

