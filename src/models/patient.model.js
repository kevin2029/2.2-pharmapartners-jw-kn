const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const AddressSchema = require("./schemas/address.schema");
const NameSchema = require("./schemas/name.schema");

const PatientScheme = new Schema({
    fullName: NameSchema,

    fullAddress: AddressSchema,

    birthDate: {
        type: Date,
        required: [true, "geboortedatum nodig."],
    },
    bsn: {
        type: String,
        required: [true, "BSN nodig."],
    },
    phoneNumber: {
        type: String,
        required: [true, "telefoonnummer nodig."],
    },
    gender: {
        type: String,
        enum: ["M", "V", "A"],
    },
});

module.exports = getmodel("Patient", PatientScheme);