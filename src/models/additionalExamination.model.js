const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const ExaminationSchema = require("./schemas/examination.schema");

const AdditionalExaminationScheme = new Schema({
    patient: {
        type: Schema.Types.ObjectId,
        ref: "patient",
        required: [true, "patient is nodig."],
    },
    cholesterolTotaal: [ExaminationSchema],
    HDLCholesterol: [ExaminationSchema],
    LDLCholesterol: [ExaminationSchema],
    triglyceriden: [ExaminationSchema],
    cholesterolHDLCholesterolRatio: [ExaminationSchema],
});

module.exports = getmodel("AdditionalExamination", AdditionalExaminationScheme);