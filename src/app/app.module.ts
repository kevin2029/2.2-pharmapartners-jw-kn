import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AlertModule } from './shared/alert';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './pages/auth/login/login.component';

import { TwofactorComponent } from './pages/auth/login/twofactor/twofactor.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CredentialsComponent } from './pages/auth/login/credentials/credentials.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LayoutComponent,
    LoginComponent,
    
    TwofactorComponent,
    CredentialsComponent
  ],
  imports: [BrowserModule, AppRoutingModule, NgbModule, HttpClientModule, AlertModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent],
})

export class AppModule {}

