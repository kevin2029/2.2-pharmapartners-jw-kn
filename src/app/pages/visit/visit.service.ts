import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap} from 'rxjs/operators';
import { Visit } from './visit.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../auth/authentication.service';


@Injectable({
  providedIn: 'root'
})
export class VisitService {
  private visiturl = 'api/visit'
  private token$ = this.authService.getToken();
  

  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
       Authorization: `${this.token$}`,
    })
  }

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) {}

  getAll(): Observable<Visit[]> {
    console.log(`Fetched visits by date: `)
    return this.http.get<Visit[]>(this.visiturl, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Fetched all visits`)),
        catchError(this.handleError<Visit[]>('getAllVisits', []))
      )
  }

  getByDate(date: string): Observable<Visit[]> {
    return this.http.get<Visit[]>(`${this.visiturl}/date/${date}`, this.httpOptions)
      .pipe(
        catchError(this.handleError<Visit[]>('getAllVisits', []))
      )
  }

  // Http Error Handling
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation, error);
      return of(result);
    }
  }
}
