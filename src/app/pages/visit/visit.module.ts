import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import * as fromComponents from '.';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardGuard } from '../auth/auth-guard.guard';

const routes: Routes = [
  { path: '',
    pathMatch: 'full',
    component: fromComponents.VisitListComponent,
    canActivate: [AuthGuardGuard]  
  },
  { path: 'aanmaken',
    pathMatch: 'full',
    component: fromComponents.VisitListComponent,
    canActivate: [AuthGuardGuard] },
]

@NgModule({
  declarations: [...fromComponents.components,],
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule,
    NgbModule
  ]
})
export class VisitModule { }
