export class Visit {
  patient: string;
  generalPractinioner: string;
  visitCard: string;
  visitAddress: string;
  date: Date;
}
