import { VisitListComponent } from './visit-list/visit-list.component';
import { VisitCreateComponent } from './visit-create/visit-create.component';

export const components: any[] = [
  VisitListComponent,
  VisitCreateComponent,
]

export * from './visit-list/visit-list.component'
export * from './visit-create/visit-create.component'
