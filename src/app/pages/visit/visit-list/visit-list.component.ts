import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Visit } from '../visit.model';
import { VisitService } from '../visit.service';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-visit-list',
  templateUrl: './visit-list.component.html',
  styleUrls: ['./visit-list.component.css']
})
export class VisitListComponent implements OnInit, OnDestroy {
  visits$: Visit[];
  model: NgbDateStruct;
  date: { year: number, month: number };
  subscription: Subscription;

  constructor(private VisitService: VisitService,
    private calendar: NgbCalendar
  ) { }

  ngOnInit(): void {
    console.log('VisitList geladen');

    window.scrollTo(0, 0);
    this.model = this.calendar.getToday();
    let d = new Date();
    const currentDate = (d.getFullYear()) + '-' +
      (d.getMonth() > 8 ? (+d.getMonth() + 1).toString() : '0' + (+d.getMonth() + 1).toString()) + '-' +
      (d.getDate() > 9 ? d.getDate() : '0' + d.getDate())
    this.subscription = this.VisitService.getByDate(currentDate).subscribe(visits => this.visits$ = visits);
  }

  change(event) {
    this.visits$ = null;
    let dateString = `${event.year}-${event.month}-${event.day}`
    let d = new Date(dateString);
    let newDate = (d.getFullYear()) + '-' +
      (d.getMonth() > 8 ? (+d.getMonth() + 1).toString() : '0' + (+d.getMonth() + 1).toString()) + '-' +
      (d.getDate() > 9 ? d.getDate() : '0' + d.getDate())
    this.subscription = this.VisitService.getByDate(newDate).subscribe(visits => this.visits$ = visits);
  }

  selectToday() {
    this.model = this.calendar.getToday();
    let newDate = new Date();
    let year = newDate.getFullYear();
    let month = newDate.getMonth();
    month++;
    let day = newDate.getDate();
    this.change({ year: year, month: month, day: day })
  }

  ngOnDestroy() {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe()
    }
  }
}
