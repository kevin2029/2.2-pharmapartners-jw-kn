import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EpisodeListComponent } from './episode-list/episode-list.component';
import { EpisodeCreateComponent } from './episode-create/episode-create.component';
import { Episode } from './episode.model';
import { AuthGuardGuard } from '../auth/auth-guard.guard';

const routes: Routes = [
  { path: 'list/:id', pathMatch: 'full', component: EpisodeListComponent, canActivate: [AuthGuardGuard] },
  { path: 'create/:id', pathMatch: 'full', component: EpisodeCreateComponent, canActivate: [AuthGuardGuard] }
]

@NgModule({
  declarations: [EpisodeCreateComponent, EpisodeListComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule,
    NgbModule
  ]
})
export class EpisodeModule { }