import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EpisodeService } from '../episode.service';

@Component({
  selector: 'app-episode-create',
  templateUrl: './episode-create.component.html',
  styleUrls: ['./episode-create.component.css']
})
export class EpisodeCreateComponent implements OnInit, OnDestroy {
  #episodeform = new FormGroup({
    date: new FormControl(''),
    s: new FormControl(''),
    o: new FormControl(''),
    e: new FormControl(''),
    p: new FormControl(''),
  })

  public id = "";
  subscription: Subscription;

  constructor(private EpisodeService: EpisodeService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = "" + this.route.snapshot.paramMap.get('id');
    window.scrollTo(0,0);
    (document.getElementById('date') as HTMLInputElement).valueAsDate = new Date();
  }

  onSubmit(): void {
    console.log("onsubmit called")
    
    const date = (document.getElementById("date") as HTMLInputElement).valueAsDate;
    const s = (document.getElementById("s") as HTMLInputElement).value;
    const o = (document.getElementById("o") as HTMLInputElement).value;
    const e = (document.getElementById("e") as HTMLInputElement).value;
    const p = (document.getElementById("p") as HTMLInputElement).value;

    const newEpisode = {
      date: date,
      s: s,
      o: o,
      e: e,
      p: p,
    }
    this.subscription = this.EpisodeService.createEpisode(this.id, newEpisode).subscribe(data => {
      if (data !== null || data !== undefined) {
        this.router.navigate([`/episodes/list/${this.id}`], {relativeTo: this.route});
      } else {
        alert("Fout bij het toevoegen, probeer het opnieuw");
      }
    }
    );
  }
  ngOnDestroy() {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe()
    }

  }

}
