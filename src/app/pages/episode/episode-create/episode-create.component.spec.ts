import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisodeCreateComponent } from './episode-create.component';

describe('EpisodeCreateComponent', () => {
  let component: EpisodeCreateComponent;
  let fixture: ComponentFixture<EpisodeCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EpisodeCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EpisodeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
