import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Visitcard } from '../../visitcard/visitcard.model';
import { Episode } from '../episode.model';
import { EpisodeService } from '../episode.service';
import { VisitcardService } from '../../visitcard/visitcard.service'


@Component({
  selector: 'app-episode-list',
  templateUrl: './episode-list.component.html',
  styleUrls: ['./episode-list.component.css']
})
export class EpisodeListComponent implements OnInit, OnDestroy {
  public patientId = "";
  public id = "";
  episodes$: Observable<Episode[]>;
  visitCard$: Visitcard;
  private subscription: Subscription;
  
  constructor(private VisitCardService: VisitcardService, private EpisodeService: EpisodeService, private route: ActivatedRoute, private location: Location) { }
  
  ngOnInit(): void {
    console.log("Episodelist geladen");
    window.scrollTo(0,0);
    this.patientId = "" + this.route.snapshot.paramMap.get('id');
    this.VisitCardService.getByPatient(this.patientId).subscribe(visitCard => {
      this.visitCard$ = visitCard;
    });
    this.episodes$ = this.EpisodeService.getEpisodes(this.patientId);
  }

  ngOnDestroy() {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe()
    }
  }

}
