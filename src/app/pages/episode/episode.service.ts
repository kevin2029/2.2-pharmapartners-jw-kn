import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Episode } from './episode.model';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class EpisodeService {
  private episodeUrl = '/api/visitcard'

  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
       Authorization: this.authService.getToken()
    })
  };

  constructor(private http: HttpClient, private authService: AuthenticationService) { }

      //get functie voor alle episodes
      getEpisodes(id: string): Observable<Episode[]> {
        return this.http.get<Episode[]>(this.episodeUrl + `/${id}/journal`, this.httpOptions)
        .pipe(
          tap(_ => console.log("fetched episodes by patient id")),
          catchError(this.handleError<Episode[]>("getEpisodes", []))
        )
      }
  
      getEpisode(id: string, place: string): Observable<Episode> {
        return this.http.get<Episode>(this.episodeUrl + `/${id}/journal/${place}`, this.httpOptions)
        .pipe(
          tap(_ => console.log(`fetched episode id=${id}`)),
          catchError(this.handleError<Episode>(`getEpisode id = ${id}`))
        );
      }

      createEpisode(id: string, episode: Episode): Observable<Episode> {      
        return this.http.put<Episode>(this.episodeUrl + "/" + id + "/journal", episode, this.httpOptions)
        .pipe(
          tap(_ => console.log(`added episode`)),
          catchError(this.handleError<Episode>(`createEpisode for id = ${id}`))
        )
      }

  // Http Error Handling
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation, error);
      console.log("error: ", error);
      
      return of(result);
    }
}
}
