import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from './authentication.service';
import { User } from './user.model';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule ]
    });
    service = TestBed.inject(AuthenticationService);
  });

  afterEach(() => {
    // Reset test objects
    service = null;
  })

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('#logout() should set currentUser to null and stop it', () => {
    // Arrange
    let user = new User();
    user.token = "test";

    // Act
    service.currentUser.next(user);
    service.currentUser.isStopped = false;
    service.logout();

    // Assert
    expect(service.currentUser.value).toEqual(null);
    expect(service.currentUser.isStopped).toEqual(true);
  });

  it('#userVal() should return latest added user', () => {
    // Arrange
    const user = new User();
    user.token = "test";

    // Act
    service.currentUser.next(user);

    // Assert 
    expect(service.userVal).toEqual(user);
  });
});
