import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/pages/auth/authentication.service';
import { User } from 'src/app/pages/auth/user.model';
import { AlertService } from 'src/app/shared/alert';

@Component({
  selector: 'app-twofactor',
  templateUrl: './twofactor.component.html',
  styleUrls: ['./twofactor.component.css']
})
export class TwofactorComponent implements OnInit {

  private _user: User;

  constructor(private router: Router, private route: ActivatedRoute, private alertService: AlertService, private authenticationService: AuthenticationService) {
  }

  failOptions = {
    autoClose: true,
    keepAfterRouteChange: false,
  }

  successOptions = {
    autoClose: true,
    keepAfterRouteChange: true,
  }

  ngOnInit(): void {
    console.log("Loading TwofactorComponent!")

    this._user = this.authenticationService?.userVal;

    if (this._user?.token) { // Check if user is logged in
      console.log("User token detected!");

      this.alertService.error('Er is al ingelogd.', this.failOptions)
      this.router.navigate(['/visits'], { relativeTo: this.route }); // Go back home
    }
    if (!this._user?.id) { // Checks if user attempted a login
      console.log("No user ID detected!");

      this.router.navigate(['../credentials'], { relativeTo: this.route }); // Back to credentials phase
    }

    console.log("User: ", this._user?.id);
  }

  onSubmit(): void {
    console.log("Attempting two factor authentication!")

    // Get field data
    const code = (document.getElementById("code") as HTMLInputElement).value;

    this.authenticationService.twoFactor(this._user.id, code).subscribe((user) => {
      if (user) {
        // If code was OK
        console.log('User ID:', user.id);

        this.alertService.success('Je bent ingelogd.', this.successOptions);
        this.router.navigate(['/visits'], { relativeTo: this.route }); // Sends user to home
      }
      else {
        // If code wasn't OK
        this.alertService.error('Code was fout.', this.failOptions);
      }
    });
  }

  ngOnDestroy(): void {
    this._user = null;

    console.log("Destroying TwofactorComponent!")
  }
}
