import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';
import { AlertService } from 'src/app/shared/alert';
import { AuthenticationService } from '../../authentication.service';
import { User } from '../../user.model';
import { TwofactorComponent } from './twofactor.component';

describe('TwofactorComponent', () => {
  let component: TwofactorComponent;
  let fixture: ComponentFixture<TwofactorComponent>;
  let authService: AuthenticationService;
  let alertService: AlertService;
  let router: Router;

  beforeEach(async () => {
    // Arrange
    TestBed.configureTestingModule({
      declarations: [ TwofactorComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule, FormsModule ]
    })
    .compileComponents()
    .then(() => {
      // Set testable objects
      fixture = TestBed.createComponent(TwofactorComponent);
      authService = TestBed.inject(AuthenticationService);
      alertService = TestBed.inject(AlertService);
      router = TestBed.inject(Router);
      component = fixture.componentInstance;

      fixture.detectChanges();
    });
  });

  afterEach(() => {
    // Reset test objects
    fixture = null;
    authService = null;
    component = null;
    alertService = null;
    router = null;
  })

  it('should create', () => {
    // Assert
    expect(component).toBeTruthy();
  });

  it('#ngOnInit() should call router navigate when User with token is returned', () => {
    // Arrange
    let user = new User();
    user.token = "test";

    authService.currentUser = new BehaviorSubject<User>(user);
    spyOn(router, 'navigate').and.returnValue(null);

    // Act
    component.ngOnInit();

    // Assert
    expect(router.navigate).toHaveBeenCalledWith(['/visits'], { relativeTo: component['route'] });
  });

  it('#ngOnInit() should call router navigate when User without id is returned', () => {
    // Arrange
    let user = new User();
    authService.currentUser = new BehaviorSubject<User>(user);

    spyOn(router, 'navigate').and.returnValue(null);

    // Act
    component.ngOnInit();

    // Assert
    expect(router.navigate).toHaveBeenCalledWith(['../credentials'], { relativeTo: component['route'] });
  });

  it('#onSubmit() should call router navigate when User is returned', () => {
    // Arrange
    let user = new User();
    user.id = "test";
    authService.currentUser = new BehaviorSubject<User>(user);

    spyOn(router, 'navigate').and.returnValue(null);
    spyOn(authService, 'twoFactor').and.returnValue(
      of(user)
    );

    // Act
    component.ngOnInit();
    component.onSubmit();

    // Assert
    expect(router.navigate).toHaveBeenCalledWith(['/visits'], { relativeTo: component['route'] });
  });

  
  it('#onSubmit() should call router navigate when Undefined is returned', () => {
    // Arrange
    let user = new User();
    user.id = "test";
    authService.currentUser = new BehaviorSubject<User>(user);
    
    spyOn(alertService, 'error');
    spyOn(authService, 'twoFactor').and.returnValue(
      of(undefined)
    );

    // Act
    component.ngOnInit();
    component.onSubmit();

    // Assert
    expect(alertService.error).toHaveBeenCalled();
  });

  it('#onSubmit() should call twoFactor of service', () => {
    // Arrange
    let user = new User();
    user.id = "test";
    authService.currentUser = new BehaviorSubject<User>(user);

    spyOn(authService, 'twoFactor').and.returnValue(
      of(user)
    );

    // Act
    component.ngOnInit();
    component.onSubmit();

    // Assert
    expect(authService.twoFactor).toHaveBeenCalled();
  });
  
  it('#ngOnDestroy() should clear user variable', () => {
    // Arrange
    spyOn(router, 'navigate').and.returnValue(null);
    spyOn(authService, 'login').and.returnValue(
      of(new User)
    );

    // Act
    component.ngOnInit();
    component.ngOnDestroy();

    // Assert
    expect(component['_user']).toEqual(null);
  });
});
