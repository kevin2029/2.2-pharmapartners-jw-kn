import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/shared/alert';
import { AuthenticationService } from '../../authentication.service';
import { User } from '../../user.model';

@Component({
  selector: 'app-credentials',
  templateUrl: './credentials.component.html',
  styleUrls: ['./credentials.component.css'],
})
export class CredentialsComponent implements OnInit {

  private _user: User;

  constructor(private router: Router, private route: ActivatedRoute, private alertService: AlertService,
    private authenticationService: AuthenticationService){
  }

  failOptions = {
    autoClose: true,
    keepAfterRouteChange: false,
  }

  successOptions = {
    autoClose: true,
    keepAfterRouteChange: true,
  }

  ngOnInit(): void {
    console.log("Loading CredentialsComponent!")

    this._user = this.authenticationService.userVal; // Get current user from service

    if(this._user?.token) { // Check if user is logged in
      console.log("User token detected!");

      this.alertService.error('Er is al ingelogd.', this.failOptions)
      this.router.navigate(['/visits'], {relativeTo: this.route}); // Go back home
    }
  }

  onSubmit(): void {
    console.log("Attempting login!")

    // Get field data
    const username = (document.getElementById("usernameid") as HTMLInputElement).value;
    const password = (document.getElementById("passwordid") as HTMLInputElement).value;
 
    this.authenticationService.login(username, password).subscribe((user) => {
      if(user) {
        // If user exists
        console.log('User ID:', user.id);

        this.alertService.success('Gegevens zijn correct.', this.successOptions);
        this.router.navigate(['../twoFactor'], {relativeTo: this.route}); // Sends user to two step verification phase
      }
      else {
        // If user doesn't exists
        this.alertService.error('Gebruikersnaam of wachtwoord is onjuist.', this.failOptions);
      }
    });
  }

  ngOnDestroy(): void {
    this._user = null;

    console.log("Destroying CredentialsComponent!")
  }

}
