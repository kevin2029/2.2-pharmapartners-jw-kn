import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthenticationService } from '../../authentication.service';
import { CredentialsComponent } from './credentials.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { BehaviorSubject, of } from 'rxjs';
import { User } from '../../user.model';
import { AlertService } from 'src/app/shared/alert';
import { Router } from '@angular/router';

describe('LoginCredentialsComponent', () => {
  let component: CredentialsComponent;
  let componentFixture: ComponentFixture<CredentialsComponent>;
  let authenticationService: AuthenticationService;
  let alertService: AlertService;
  let router: Router;

  beforeEach(async () => {
    // Arrange
    TestBed.configureTestingModule({
      declarations: [ CredentialsComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule, FormsModule ]
    })
    .compileComponents()
    .then(() => {
      // Set testable objects
      component = componentFixture.componentInstance;
      componentFixture = TestBed.createComponent(CredentialsComponent);
      authenticationService = TestBed.inject(AuthenticationService);
      alertService = TestBed.inject(AlertService);
      router = TestBed.inject(Router);


      componentFixture.detectChanges();
    });
  });

  afterEach(() => {
    // Reset
    component = null;
    componentFixture = null;
    authenticationService = null;
    alertService = null;
    router = null;
  })

  it('#ngOnInit() should call router navigate when User with token is returned', () => {
    // Arrange
    let user = new User();
    user.token = "test";

    authenticationService.currentUser = new BehaviorSubject<User>(user);
    spyOn(router, 'navigate').and.returnValue(null);

    // Act
    component.ngOnInit();

    // Assert
    expect(router.navigate).toHaveBeenCalled();
  });

  it('#ngOnInit() should set _user when valid user is given from authService', () => {
    // Arrange
    const user = new User();
    user.token = "test";
    authenticationService.currentUser = new BehaviorSubject<User>(user);

    // Act
    component.ngOnInit();

    // Assert
    expect(component['_user']).toEqual(user);
  });
  
  it('#onSubmit() should throw error on undefined user', () => {
    // Arrange
    spyOn(alertService, 'error');
    spyOn(authenticationService, 'login').and.returnValue(
      of(undefined)
    );

    // Act
    component.ngOnInit();
    component.onSubmit();

    // Assert
    expect(alertService.error).toHaveBeenCalled();
  });

  it('#onSubmit() should call login of service', () => {
    // Arrange
    spyOn(authenticationService, 'login').and.returnValue(
      of(new User())
    );

    // Act
    component.ngOnInit();
    component.onSubmit();

    // Assert
    expect(authenticationService.login).toHaveBeenCalled();
  });

  it('#onSubmit() should call router navigate when user is returned', () => {
    // Arrange
    spyOn(router, 'navigate').and.returnValue(null);
    spyOn(authenticationService, 'login').and.returnValue(
      of(new User)
    );

    // Act
    component.ngOnInit();
    component.onSubmit();

    // Assert
    expect(router.navigate).toHaveBeenCalled();
  });

  it('#ngOnDestroy() should clear user variable', () => {
    // Arrange
    spyOn(router, 'navigate').and.returnValue(null);
    spyOn(authenticationService, 'login').and.returnValue(
      of(new User)
    );

    // Act
    component.ngOnInit();
    component.ngOnDestroy();

    // Assert
    expect(component['_user']).toEqual(null);
  });
});
