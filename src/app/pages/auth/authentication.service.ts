import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user.model';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private authUrl = 'api/authentication'
  public currentUser = new BehaviorSubject<User>(undefined);
  public user: Observable<User>;

  private readonly CURRENT_USER = 'currentuser';

  private readonly httpOptions= {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  }

  alertOptions= {
    autoClose: true,
    keepAfterRouteChange: true,
  }

  constructor(private http: HttpClient, private alertService: AlertService, private router: Router)
  {
    this.currentUser = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(this.CURRENT_USER)));
    this.user = this.currentUser.asObservable();
  }

  // Getter for user value
  public get userVal(): User {
    return this.currentUser.value;
  }

  login(username: string, password: string): Observable<User> {
    console.log(`login at ${this.authUrl}/login`);

    return this.http
      .post(
        `${this.authUrl}/login`,
        { username, password },
        this.httpOptions
      )
      .pipe(
        map((response: any) => {
          const user = { ...response } as User;
          this.currentUser.next(user);
          return user;
        }),
        catchError((error: any) => {
          console.log('error', error);
          console.log('error.message:', error.message);
          console.log('error.error.message:', error.error.message);
          return of(undefined);
        })
      )
  }
   Options = {
    autoClose: true,
    keepAfterRouteChange: true,
  }

  logout(): void {
    console.log("Attempting logout!");
    localStorage.removeItem(this.CURRENT_USER)
    this.currentUser.isStopped = true;
    this.currentUser.next(null);
    this.alertService.success("U bent succesvol uitgelogd.", this.Options)
    this.router.navigate(['/']); 
  }

  twoFactor(id: string, code: string): Observable<User> {
    console.log(`login at ${this.authUrl}/validate`);

    return this.http
    .post(
      `${this.authUrl}/validate/${id}`,
      { "code": code },
      this.httpOptions
    )
    .pipe(
      map((response: any) => {
        const user = { ...response } as User;
        this.saveUserToLocalStorage(user);
        this.currentUser.next(user);
        this.currentUser.isStopped = false;
        return user;
      }),
      catchError((err: any) => {
        console.log('error', err);
        return of(undefined);
      })
    )
  }

  getUserFromLocalStorage(): Observable<User> {
    if(this.currentUser.isStopped === true){
      return of(undefined)
    } else {
      const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER))
      return of(localUser);
    }
  }

  private saveUserToLocalStorage(user: User): void {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user))
  }

  register(username: string, password: string): Observable<User> {
    return this.http
      .post(
        `${this.authUrl}/register`,
        { username, password },
        this.httpOptions
      )
      .pipe(
        map((response: any) => {
          const user = { ...response } as User;
          this.saveUserToLocalStorage(user);
          this.currentUser.next(user);
          this.alertService.success('Gebruiker aangemaakt en ingelogd.', this.alertOptions);
          return user;
        }),
        catchError((error: any) => {
          console.log('error', error);
          console.log('error.message:', error.message);
          console.log('error.error.message:', error.error.message);
          return of(undefined)
        })
      )
  } 

  
  getToken(): string {
    if(localStorage.getItem(this.CURRENT_USER) != null){
       const token = JSON.parse(localStorage.getItem(this.CURRENT_USER)).token
       return token
      }else{
        return "null"
      }
  }
}
