import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Medicine } from './medicine.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MedicationInformation } from './medicationInformation.model';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class MedicineService {
private medicineurl = "/api/visitcard"
private token$ = this.authService.getToken();
  

  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
       Authorization: `${this.token$}`,
    })
  }

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) { }

  getMedicationInformation() : Observable<MedicationInformation[]> {
    return this.http.get<MedicationInformation[]>(`api/medicationinformation`, this.httpOptions)
    .pipe(
      tap( _ => console.log('get medicationInformation')),
      catchError(this.handleError<any>("get medicationInformation"))
    )
  }

 getAll(id: string): Observable<Medicine[]> {
    return this.http.get<Medicine[]>(`${this.medicineurl}/${id}/medication`, this.httpOptions)
    .pipe(
      tap( _ => console.log('get Medicine')),
       catchError(this.handleError<any>('get Medicine'))
    ) 
  }

  getById(id: string): Observable<Medicine> {
   return this.http.get<Medicine>(`${this.medicineurl}/${id}`, this.httpOptions).pipe(
      tap( _ => console.log('get Medicine')),
       catchError(this.handleError<any>('get Medicine by id'))
    ) 
   
  }

  delete(id: string): Observable<Medicine> {
   const url = `${this.medicineurl}/${id}`  
    return this.http.delete<Medicine>(url, this.httpOptions).pipe(
      tap( _ => console.log('Medicine deleted')),
      catchError(this.handleError<any>('delete Medicine'))
      )
  }

  create(id: string, medicine: Medicine): Observable<Medicine> {
    return this.http.put<Medicine>(`${this.medicineurl}/${id}/medication`, medicine, this.httpOptions)
    .pipe(
      tap( _ => console.log('create medicine')),
      catchError(this.handleError<any>('create Medicine'))
    ) 

  }
 update(medicine: Medicine): Observable<any> {
   const url = `${this.medicineurl}/${medicine}`
   return this.http.put(url, medicine, this.httpOptions).pipe(
     tap( _ => console.log('Medicine updated')),
     catchError(this.handleError<any>('update Medicine'))
   )
  }
 private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result);
    };
  }
}
