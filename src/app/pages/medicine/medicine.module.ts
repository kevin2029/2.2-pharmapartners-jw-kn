import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedicineCreateComponent} from './medicine-create/medicine-create.component';
import { MedicineListComponent} from './medicine-list/medicine-list.component';
import { Routes, RouterModule} from '@angular/router';

import { FormsModule} from '@angular/forms'
import { AuthGuardGuard } from '../auth/auth-guard.guard';

const routes: Routes = [
  {
    path: 'list/:id',
    pathMatch: 'full,',
    component: MedicineListComponent,
    canActivate: [AuthGuardGuard]
  },

  {
    path: 'add/:id',
    pathMatch: 'full',
    component: MedicineCreateComponent,
    canActivate: [AuthGuardGuard]
  },

]



@NgModule({
  declarations: [MedicineListComponent, MedicineCreateComponent ],
  imports: [
    CommonModule, RouterModule.forChild(routes), FormsModule
  ],
})
export class MedicineModule { }
