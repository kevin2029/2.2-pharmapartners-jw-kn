import { Component, OnDestroy, OnInit } from '@angular/core';
import { MedicineService } from '../medicine.service';
import { FormControl, FormGroup } from '@angular/forms';
import { MedicationInformation } from '../medicationInformation.model';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-medicine-create',
  templateUrl: './medicine-create.component.html',
  styleUrls: ['./medicine-create.component.css']
})
export class MedicineCreateComponent implements OnInit, OnDestroy {
  #medicineform = new FormGroup({
    medicationTitle: new FormControl(''),
    addition: new FormControl(''),
    concentration: new FormControl(''),
    duration: new FormControl(''),
    selectorBtnMg: new FormControl(''),
    selectorBtnMl: new FormControl('')
  })

  private mgBool = false;
  private mlBool = false;
  public id = "";
  medicineList$: Observable<MedicationInformation[]>;
  subscription: Subscription;

  constructor(private router: Router, private MedicineService: MedicineService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = "" + this.route.snapshot.paramMap.get('id');
    window.scrollTo(0,0);
    this.medicineList$ = this.MedicineService.getMedicationInformation();
  }

  onSubmit(): void {
    console.log("onsubmit called")
    
    const medicationTitle = (document.getElementById("medicationTitle") as HTMLInputElement).value;
    const addition = (document.getElementById("addition") as HTMLInputElement).value;
    let concentration = (document.getElementById("concentration") as HTMLInputElement).value;
    const duration = (document.getElementById("duration") as HTMLInputElement).value;
    const mgBool = (document.getElementById("selectorBtnMg") as HTMLInputElement).value;
    const mlBool = (document.getElementById("selectorBtnMl") as HTMLInputElement).value;

    if (this.mgBool) {
      concentration += "mg"
    } else if (this.mlBool) {
      concentration += "ml"
    }
    const newMedication = {
      medicationTitle: medicationTitle,
      addition: addition,
      concentrations: concentration,
      duration: duration
    }
    this.subscription = this.MedicineService.create(this.id, newMedication).subscribe(data => {
      if (data !== null || data !== undefined) {
        this.router.navigate([`/medicine/list/${this.id}`], { relativeTo: this.route });
      } else {
        alert("Fout bij het toevoegen, probeer het opnieuw");
      }
    });
  }
  ngOnDestroy() {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe()
    }
  }

  onClickMg(): void {
    this.mgBool = true;
    this.mlBool = false;
  }

  onClickMl(): void {
    this.mlBool = true;
    this.mgBool = false;
  }

}


