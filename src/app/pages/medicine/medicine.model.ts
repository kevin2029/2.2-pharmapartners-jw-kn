export class Medicine{
    medicationTitle: string;
    addition: string;
    concentrations: string;
    duration: string;
}