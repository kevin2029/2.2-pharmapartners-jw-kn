import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Visitcard } from '../../visitcard/visitcard.model';
import { VisitcardService } from '../../visitcard/visitcard.service';
import { Medicine } from '../medicine.model';
import { MedicineService } from '../medicine.service'

@Component({
  selector: 'app-medicine-list',
  templateUrl: './medicine-list.component.html',
  styleUrls: ['./medicine-list.component.css']
})
export class MedicineListComponent implements OnInit, OnDestroy {
  public id: string;
  public patientId = "";
  private subscription: Subscription;
  medicine$: Observable<Medicine[]>;
  visitCard$: Visitcard;

  constructor(private VisitCardService: VisitcardService, private MedicineService: MedicineService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log('Lijst met medicijnen geladen');
    window.scrollTo(0,0);
    this.patientId = "" + this.route.snapshot.paramMap.get('id');
    this.VisitCardService.getByPatient(this.patientId).subscribe(visitCard => {
      this.visitCard$ = visitCard;
    });
    this.medicine$ = this.MedicineService.getAll(this.patientId);
  }

  ngOnDestroy() {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe()
    }
  }


}
