import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Visitcard } from './visitcard.model';
import { VisitcardService } from './visitcard.service'
import { Medicine } from '../medicine/medicine.model'
import { Episode } from '../episode/episode.model'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-visitcard',
  templateUrl: './visitcard.component.html',
  styleUrls: ['./visitcard.component.css']
})
export class VisitcardComponent implements OnInit {
  visitCard$: Visitcard;
  medicines$: Medicine[];
  episode$: Observable<Episode[]>;

  constructor(private visitcardService: VisitcardService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log("Visitcard geladen");
    window.scrollTo(0,0);
    const id = "" + this.route.snapshot.paramMap.get('id');
    this.visitcardService.getVisitCard(id).subscribe(visitCard => {
      this.visitCard$ = visitCard;
    });

  }

}
