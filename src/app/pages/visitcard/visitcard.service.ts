import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Visitcard} from './visitcard.model'
import { catchError, map, tap } from 'rxjs/operators';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class VisitcardService {
   private visitcardurl = 'api/visitcard'

  private token$ = this.authService.getToken();
  
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
       Authorization: `${this.token$}`,
    })
  }


  constructor(private http: HttpClient, private authService: AuthenticationService) { }

  
      getVisitCard(id: string): Observable<Visitcard> {
        return this.http.get<Visitcard>(this.visitcardurl + `/${id}`, this.httpOptions)
        .pipe(
          tap(_ => console.log(`fetched Visitcard id=${id}`)),
          catchError(this.handleError<Visitcard>(`getvisitcard id = ${id}`))
        );
      }

      createvisitcard(id: string, visitcard: Visitcard): void {
        console.log("createVisitcard called, Visitcard: ", visitcard);
        this.http.put<Visitcard>(this.visitcardurl + `/${id}/journal`, visitcard, this.httpOptions)
        .pipe(
          tap(_ => console.log(`added Visitcard: ${visitcard}`)),
          catchError(this.handleError<Visitcard>(`createVisitcard for id = ${id}`))
        )
      }

      getByPatient(id: string): Observable<Visitcard> {
        console.log("getByPatient called, patientId: ", id);
        return this.http.get<Visitcard>(`${this.visitcardurl}/p/${id}`, this.httpOptions)
        .pipe(
          tap(_ => console.log(`fetched VisitcardId from patient: ${id}`)),
          catchError(this.handleError<Visitcard>(`VisitcardId from patient: ${id}`))
        )
      }


       private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation, error);
      return of(result);
    }
}
}
