export class Visitcard {
    _id: string
    patient: string;
    medication: Array<any>;
    journal: Array<any>;
    patientName: string;
} 