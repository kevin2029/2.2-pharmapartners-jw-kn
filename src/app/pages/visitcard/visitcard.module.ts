import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router';
import { FormsModule} from '@angular/forms'
import { VisitcardComponent} from './visitcard.component'
import { AuthGuardGuard } from '../auth/auth-guard.guard';

const routes: Routes = [

  {
    path: '',
    pathMatch: 'full',
    component: VisitcardComponent,
    canActivate:[AuthGuardGuard]
  },

]



@NgModule({
  declarations: [VisitcardComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), FormsModule
  ],
})
export class VisitcardModule { }
