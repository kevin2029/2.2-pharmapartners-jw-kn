import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/pages/auth/authentication.service';
import { User } from '../../pages/auth/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  isNavbarCollapsed = true;
  loggedInUser$: Observable<User>;

  constructor(private authService: AuthenticationService) {}

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.getUserFromLocalStorage()
  }

  logout(): void {
    this.authService.logout();
    this.loggedInUser$ = this.authService.getUserFromLocalStorage()
  }
}
