import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/auth/login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { CredentialsComponent } from './pages/auth/login/credentials/credentials.component';
import { TwofactorComponent } from './pages/auth/login/twofactor/twofactor.component';

const routes: Routes = [
  { 
    path: 'login', 
    component: LoginComponent,
    children: [
      {
        path: '', 
        redirectTo: 'credentials', 
        pathMatch: 'full'
      },
      {
        path: 'credentials',
        component: CredentialsComponent,
        pathMatch: 'full'
      },
      {
        path: 'twoFactor',
        component: TwofactorComponent,
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/login/credentials' },
      { path: 'visits',
        loadChildren: () =>
          import('./pages/visit/visit.module').then((m) => m.VisitModule)},
      { path: 'episodes',
       loadChildren: () => import('./pages/episode/episode.module').then((m) => m.EpisodeModule)},
      { path: 'medicine',
        loadChildren: () =>
          import('./pages/medicine/medicine.module').then((m) => m.MedicineModule)},
      { path: 'visitcard/:id',
        loadChildren: () =>
          import('./pages/visitcard/visitcard.module').then((m) => m.VisitcardModule)}
    
    ],
  },

  { path: '**', redirectTo: '/login/credentials' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {}
