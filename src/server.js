require("dotenv").config();
const express = require("express");
const path = require("path");
const http = require("http");
const connect = require("./connect");
const bodyParser = require("body-parser");
const compression = require("compression");
const visitroutes = require("../src/routes/visit.route");
const visitcardroutes = require("./routes/visitcard.route");
const medicationinformationroutes = require("./routes/medicationinformation.route");
const gproutes = require("./routes/gp.route");
const patientroutes = require("./routes/patient.route");
const Auditor = require("./controllers/auditor");
const log = require("simple-node-logger").createSimpleLogger("logs.log");
const authenticationRoutes = require("./routes/authentication.route");
const logRoutes = require("./routes/log.route");
const physicalExaminationRoutes = require("./routes/physicalExamination.route");
const additionalExaminationRoutes = require("./routes/additionalExamination.route");

const app = express();

app.use(bodyParser.json());
app.use(compression());

//
// appname is the name of the "defaultProject" value that was set in the angular.json file.
// When built in production mode using 'ng build --prod', a ./dist/{appname} folder is
// created, containing the generated application. The appname points to that folder.
//
// Replace the name below to match your own "defaultProject" value!
//
const appname = "pharmapartners";

// Point static path to dist
app.use(express.static(path.join(__dirname, "..", "dist", appname)));

// Logging requests that come in for debugging purposes
app.all("*", (req, res, next) => {
    const method = req.method;
    const url = req.url;
    console.log("The used method is: ", method, " on url: ", url);
    next();
});

// Catching known routes
app.use("/api/authentication", authenticationRoutes);
app.use("/api/visit", visitroutes);
app.use("/api/visitcard", visitcardroutes);
app.use("/api/medicationinformation", medicationinformationroutes);
app.use("/api/gp", gproutes);
app.use("/api/patient", patientroutes);
app.use("/api/log", logRoutes);
app.use("/api/physicalExamination", physicalExaminationRoutes);
app.use("/api/additionalExamination", additionalExaminationRoutes);

// Catch all routes and return the index file
app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "..", "dist", appname, "index.html"));
});

// Get port from environment and store in Express.
const port = process.env.PORT || "4200";
app.set("port", port);
// Create HTTP server.
const server = http.createServer(app);
// Listen on provided port, on all network interfaces.
server.listen(port, () => {
    console.log(
        `Angular app \'${appname}\' running in ${process.env.NODE_ENV} mode on port ${port}`
    );
});

connect.mongo(process.env.MONGO_PROD_DB);