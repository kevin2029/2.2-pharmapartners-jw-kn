const log = require("simple-node-logger").createSimpleLogger("logs.log");
const fullNameRepository = require("../controllers/repository/fullName.respository");

class VisitController {
    constructor(model) {
        this.model = model;
    }
    getByDate = async(req, res, next) => {
        log.info("Getting all visits of date: ", req.params.date);

        const sourceDate = new Date(req.params.date);

        const dateTmow = new Date(sourceDate);
        dateTmow.setDate(sourceDate.getDate() + 1);

        const docs = await this.model
            .find({
                date: {
                    $gte: sourceDate,
                    $lt: dateTmow,
                },
            })
            .catch();

        if (docs.length === 0) {
            res.status(200).end();
        } else {
            for (let index = 0; index < docs.length; index++) {
                const element = docs[index];
                const addressObj = element.destinationAddress;
                const addressStr = `${addressObj.street} ${addressObj.houseNumber}, ${addressObj.city}`;

                let patientName = "";
                await fullNameRepository.getPatientByname(
                    docs[index].patient,
                    (callback, err) => {
                        if (err) {
                            patientName = "No name";
                        }
                        if (callback) {
                            patientName = callback;
                        }
                    }
                );

                const newVCId = "" + docs[index].visitCard;
                const newElement = {
                    _id: docs[index]._id,
                    patient: patientName,
                    generalPractitioner: docs[index].generalPractitioner,
                    visitCard: newVCId,
                    destinationAddress: addressStr,
                    date: docs[index].date,
                };
                docs[index] = newElement;
            }
            res.status(200).send(docs);
        }
    };
}
module.exports = VisitController;