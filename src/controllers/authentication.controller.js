const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const saltRounds = 10;
const Speakeasy = require("speakeasy");
var nodemailer = require("nodemailer");
const log = require("simple-node-logger").createSimpleLogger("logs.log");

const User = require("../models/gp.model")();

var ObjectId = require("mongodb").ObjectID;

var transporter = nodemailer.createTransport({
    host: `${process.env.service}`,
    auth: {
        user: `${process.env.user}`,
        pass: `${process.env.pass}`,
    },
});
async function login(req, res, next) {
    let dbresult = await User.find({ username: req.body.username });

    // checking if there is a result from the db
    if (dbresult.length === 0) {
        console.log("User not found or password is invalid");
        res.status(400).json({
            error: "User not found or password is invalid",
            datetime: new Date().toISOString(),
        });
    } else {
        // bcrypt unhashing the password
        bcrypt.compare(
            req.body.password,
            dbresult[0].password,
            function(err, result) {
                if (err) {
                    console.log(err);
                    res.status(400).json({
                        error: "User not found or password is invalid",
                        datetime: new Date().toISOString(),
                    });
                }

                if (result == false) {
                    res.status(400).json({
                        error: "User not found or password is invalid",
                        datetime: new Date().toISOString(),
                    });
                }
                if (result) {
                    console.log("passwords DID match, generating secretnumber");

                    var secret = Speakeasy.generateSecret({ length: 20 }).base32;
                    var code;

                    //Puts the temp Hash in db
                    User.findOneAndUpdate({ username: dbresult[0].username }, {
                            tempSecret: secret,
                        },
                        function(error, result) {
                            if (error) console.log(error);
                            else {
                                log.info("Temp secret updated for: ", dbresult[0]._id);
                            }
                        }
                        //makes a readable digit for user
                    ).then(() => {
                        code = Speakeasy.totp({
                            secret: secret,
                            encoding: "base32",
                            window: 1,
                            step: 500,
                        });

                        //sends the code to the user
                        sendEmail(dbresult[0].email, code, (callback) => {
                            if (callback === null) {
                                log.info("sending verification code to: ", dbresult[0]._id);
                                res.status(200).json({
                                    succes: "on to validation",
                                    id: dbresult[0]._id,
                                });
                            } else {
                                res.status(404).json({
                                    error: "something went wrong",
                                });
                            }
                        });
                    });
                }
            }
        );
    }
}

async function register(req, res) {
    console.log("register");
    let check = await User.find({ username: req.body.username });

    if (check.length !== 0) {
        console.log("A user with username: ", req.body.username, " already exists");
        res.status(500).json({
            error: "Username is already taken",
            datetime: new Date().toISOString(),
        });
    } else {
        // bcrypt hashing the password
        console.log("bcrypt hasing the password");
        bcrypt.genSalt(saltRounds, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
                console.log("password hashed: ", hash);

                let created = new User(req.body);
                created.password = hash;
                created.tempSecret = "temp";
                created.save();

                console.log("user added");
                res.status(200).json({
                    message: "user added",
                });
            });
        });
    }
}

async function validateToken(req, res, next) {
    console.log("validateToken called");
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        console.log("No auth header!");
        res.status(401).json({
            error: "Authorization header missing!",
            datetime: new Date().toISOString(),
        });
    } else {
        jwt.verify(authHeader, "secret", (err, payload) => {
            if (err) {
                console.log("Not authorized");
                res.status(401).json({
                    error: "Not authorized",
                    datetime: new Date().toISOString(),
                });
            }
            if (payload) {
                console.log("token is valid");
                // User heeft toegang. Voeg UserId uit payload toe aan
                // request, voor ieder volgend endpoint.
                req.id = payload.id;
                next();
            }
        });
    }
}

async function SignToken(req, res) {
    log.info("signToken for: ", "id:", req.id);
    let payload = {
        ID: req.id,
    };
    // Create an object containing the data we want in the payload.
    // Userinfo returned to the caller.
    const userinfo = {
        token: jwt.sign(payload, "secret", {
            expiresIn: "2h",
        }),
        id: req.id,
        username: req.username,
    };
    res.status(200).json(userinfo);
}

function sendEmail(email, code, callback) {
    var mailOptions = {
        from: "PHPartnersBot@gmail.com",
        to: email,
        subject: "Inloggen PharmaPartners",
        text: "Uw verificatie code is:" + code,
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log("error:", error);
            callback(error);
        } else {
            console.log("Email sent: " + info.response);
            callback(null);
        }
    });
}

async function validatecode(req, res, next) {
    console.log("in validation", req.params.id);

    var id = req.params.id;

    //gets the temp secret from db
    var secret = await User.findById(ObjectId(id), function(error, result) {
        if (error) console.log(error);
        else {
            console.log("Validate code is right");
        }
    });

    //validates the code with the input
    var value = Speakeasy.totp.verify({
        secret: secret.tempSecret,
        encoding: "base32",
        token: req.body.code,
        window: 1,
        step: 500,
    });

    if (value) {
        console.log("code is right");
        req.userame = secret.username;
        req.id = req.params.id;
        next();
    } else {
        res.status(404).json({
            message: "code is not right",
        });
    }
}

module.exports = {
    login,
    register,
    validateToken,
    validatecode,
    SignToken,
};