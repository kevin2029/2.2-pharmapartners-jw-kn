const log = require("simple-node-logger").createSimpleLogger("logs.log");
const MedicationInformation = require("../models/medicationinformation.model")();
const fullNameRepository = require("../controllers/repository/fullName.respository");
const loggingRepository = require("../controllers/repository/logging.repository");

class VisitcardController {
    constructor(model) {
        this.model = model;
    }
    getOne = async(req, res, next) => {
        log.info("Getting visitcard from user: ", req.params.id);
        const entities = await this.model.find({ _id: req.params.id });
        if (entities.length === 0) {
            res.status(404).send({
                message: "Not found",
            });
        } else {
            const card = entities[0];
            const patientId = card.patient;
            let patientName = "";
            await fullNameRepository.getPatientByname(patientId, (callback, err) => {
                if (err) {
                    patientName = "No name";
                }
                if (callback) {
                    patientName = callback;
                }
            });
            const newCard = {
                _id: card._id,
                medication: card.medication,
                patient: card.patient,
                journal: card.journal,
                patientName: patientName,
            };
            res.status(200).send(newCard);
        }
    };
    getOneContent = async(req, res, next) => {
        log.info(`Getting ${req.params.type} from user: `, req.params.id);
        console.log("getOneContent called");
        const entities = await this.model.find({ patient: req.params.id });
        let type;
        let result = "";
        try {
            type = entities[0][req.params.type].reverse();
            if (req.params.type == "medication") {
                for (let index = 0; index < type.length; index++) {
                    const element = type[index];
                    const medicationObject = await MedicationInformation.findById(
                        element.medicationTitle
                    );
                    const medicationTitle = medicationObject.title;
                    const newElement = {
                        _id: type[index]._id,
                        medicationTitle: medicationTitle,
                        addition: type[index].addition,
                        concentrations: type[index].concentrations,
                        duration: type[index].duration,
                    };
                    type[index] = newElement;
                }
            }
            result = type[req.params.place];
            res.status(200).send(result);
        } catch (error) {
            log.info("Error selecting type of request");
            res.status(400).send({
                error: "error selecting type of request, check request url",
            });
        }
    };
    getAll = async(req, res, next) => {
        console.log("getAll called");
        log.info(`Getting ${req.params.type} of user: `, req.params.id);
        const entities = await this.model.find({ patient: req.params.id });
        if (entities.length === 0) {
            res.status(404).send({
                message: "Not found",
            });
        } else {
            let type = null;
            switch (req.params.type) {
                case "journal":
                    type = entities[0].journal.reverse();
                    res.status(200).send(type);
                    break;
                case "medication":
                    type = entities[0].medication.reverse();
                    for (let index = 0; index < type.length; index++) {
                        const element = type[index];
                        const medicationObject = await MedicationInformation.findById(
                            element.medicationTitle
                        );
                        const medicationTitle = medicationObject.title;
                        const newElement = {
                            _id: type[index]._id,
                            medicationTitle: medicationTitle,
                            addition: type[index].addition,
                            concentrations: type[index].concentrations,
                            duration: type[index].duration,
                        };
                        type[index] = newElement;
                    }
                    res.status(200).send(type);
                    break;
                case "currentProblems":
                    type = entities[0].currentProblems.reverse();
                    res.status(200).send(type);
                    break;
                case "history":
                    type = entities[0].history.reverse();
                    res.status(200).send(type);
                    break;
                default:
                    log.info("Error selecting type of request");
                    res.status(400).send({
                        error: "error selecting type of request, check request url",
                    });
            }
        }
    };
}
module.exports = VisitcardController;