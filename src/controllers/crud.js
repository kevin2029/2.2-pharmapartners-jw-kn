const log = require("simple-node-logger").createSimpleLogger("logs.log");
const loggingRepository = require("../controllers/repository/logging.repository");
const MedicationInformation = require("../models/medicationinformation.model")();

class CrudController {
    constructor(model) {
        this.model = model;
    }
    create = async(req, res, next) => {
        const entity = new this.model(req.body);
        await entity.save();
        log.info("Created With ID: ", entity.id);
        loggingRepository.uploadLog(`Created entity with id: ${entity.id}`);
        res.status(201).json({ id: entity.id });
    };

    getAll = async(req, res, next) => {
        console.log("Get all called");
        const entities = await this.model.find();
        res.status(200).send(entities);
    };

    getOne = async(req, res, next) => {
        const entity = await this.model.findById(req.params.id);
        res.status(200).send(entity[0]);
    };

    update = async(req, res, next) => {
        await this.model.findByIdAndUpdate(req.params.id, req.body);
        log.info("Updated With ID: ", entity.id);
        loggingRepository.uploadLog(`Updated entity with id: ${entity.id}`);
        res.status(204).end();
    };

    delete = async(req, res, next) => {
        // this happens in two steps to make mongoose middleware run
        const entity = await this.model.findById(req.params.id);
        log.info("Deleted With ID: ", entity.id);
        loggingRepository.uploadLog(`Deleted entity with id: ${entity.id}`);
        await entity.delete();
        res.status(204).end();
    };
    getByPatient = async(req, res, next) => {
        log.info("Getting physicalExamination from patient: ", req.params.id);
        const entities = await this.model.find({ patient: req.params.id });
        const examination = entities[0];
        res.status(200).send(examination);
    };
    createContent = async(req, res, next) => {
        log.info(
            "adding new: ",
            req.params.type,
            " with patient id: ",
            req.params.id,
            " req.body: ",
            req.body
        );
        const createType = req.params.type;
        let body = req.body;

        this.model.findOneAndUpdate({ patient: req.params.id }, {
                $push: {
                    [createType]: body,
                },
            }, { upsert: true, new: true },
            function(error, result) {
                if (error) log.info(error);
                else {
                    log.info("Alert updated", result.patient);
                    loggingRepository.uploadLog(
                        `Created: ${req.params.type} with patient id: ${req.params.id}`
                    );
                }
            }
        );
        res.status(204).end();
    };
    getOneContent = async(req, res, next) => {
        log.info(`Getting ${req.params.type} from patient: `, req.params.id);
        console.log("getOneContent called");
        const entities = await this.model.find({ patient: req.params.id });
        try {
            const type = entities[0][req.params.type].reverse();
            const result = type[req.params.place];
            res.status(200).send(result);
        } catch (error) {
            log.info("Error selecting type of request");
            res.status(400).send({
                error: "error selecting type of request, check request url",
            });
        }
    };
    getAllContent = async(req, res, next) => {
        console.log("getAll called");
        log.info(`Getting ${req.params.type} of user: `, req.params.id);
        const entities = await this.model.find({ patient: req.params.id });

        try {
            res.status(200).send(entities[0][req.params.type].reverse());
        } catch (error) {
            log.info("Error selecting type of request");
            res.status(400).send({
                error: "error selecting type of request, check request url",
            });
        }
    };
    updateContent = async(req, res, next) => {
        log.info("updated ", req.params.type, " with patient id: ", req.params.id);
        let place = req.params.place;
        let createType = req.params.type;
        const array = await this.model.findOne({ patient: req.params.id });
        let body = req.body;

        if (createType === "medication") {
            const medicationObject = await MedicationInformation.findOne({
                title: req.body.medicationTitle,
            });
            body = {
                medicationTitle: medicationObject._id,
                addition: req.body.addition,
                concentrations: req.body.concentrations,
                duration: req.body.duration,
            };
        }

        try {
            place = array[createType].length - place - 1;
            createType += `.${place}`;

            this.model.findOneAndUpdate({ patient: req.params.id }, {
                    [createType]: body,
                }, { upsert: true, new: true },
                function(error, result) {
                    if (error) {
                        log.info(error);
                        res.status(400).send({
                            error: "error, check request url",
                        });
                    } else {
                        log.info("Alert updated ", req.params.id);
                        loggingRepository.uploadLog(
                            `updated: ${req.params.type} with patient id: ${req.params.id}`
                        );
                        res.status(204).end();
                    }
                }
            );
        } catch (error) {
            res.status(400).send({ error: error });
        }
    };
    deleteContent = async(req, res, next) => {
        log.info("deleted ", req.params.type, " with patient id: ", req.params.id);
        const place = req.params.place;
        const filter = `${req.params.type}.${place}._id`;
        let elementId;
        let createType = req.params.type;
        const array = await this.model.findOne({ patient: req.params.id });
        try {
            const reverseStep = array[createType].reverse();
            elementId = reverseStep[place]._id;
            this.model.findOneAndUpdate({ patient: req.params.id }, {
                    $pull: {
                        [createType]: { _id: [elementId] },
                    },
                }, { upsert: true, new: true, multi: false },
                function(error, result) {
                    if (error) {
                        log.info(error);
                        res.status(400).send({
                            error: "error, check request url",
                        });
                    } else {
                        log.info("Alert updated", result.patient);
                        loggingRepository.uploadLog(
                            `Deleted: ${req.params.type} with patient id: ${req.params.id}`
                        );
                        res.status(204).end();
                    }
                }
            );
        } catch (error) {
            res.status(400).send({
                error: "error selecting type of request, check request url",
            });
        }
    };
    getLog = async(req, res, next) => {
        console.log("Get log called");
        const entities = await this.model.find();
        res.status(200).send(entities.reverse());
    };
}

module.exports = CrudController;