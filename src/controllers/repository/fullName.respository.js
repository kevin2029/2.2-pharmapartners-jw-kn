const PatientModel = require("../../models/patient.model")();
let repository = {
    async getPatientByname(id, callback, err) {
        console.log("getting name for: ", id);
        const ResName = await PatientModel.findById(id);

        let name = "";
        if (ResName === undefined) {
            err("No name");
        } else {
            if (ResName.middleName === undefined) {
                name = `${ResName.fullName.firstName} ${ResName.fullName.lastName}`;
            } else {
                name = `${ResName.fullName.firstName} ${ResName.fullName.middleName} ${ResName.fullName.lastName}`;
            }
            callback(name);
        }
    },
};

module.exports = repository;