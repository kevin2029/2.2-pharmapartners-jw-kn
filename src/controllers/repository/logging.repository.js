const loggingModel = require("../../models/logging.model")();
let repository = {
    async uploadLog(message) {
        console.log("logging: ", message);

        let today = new Date();

        let date =
            today.getDate() +
            "-" +
            (today.getMonth() + 1) +
            "-" +
            today.getFullYear();

        const newLog = {
            message: message,
            dateCreated: date,
        };
        const entity = new loggingModel(newLog);
        await entity.save();
    },
};

module.exports = repository;