module.exports = class Auditor {
    constructor(fileLogger) {
        this.fileLogger = fileLogger;
    }

    audit(action) {
        console.log(action);
        this.fileLogger.info(action);
    }
}