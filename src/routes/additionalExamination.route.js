const express = require("express");
const router = express.Router();

const AdditionalExamination = require("../models/additionalExamination.model")();
const CrudController = require("../controllers/crud");
const AdditionalExaminationCrud = new CrudController(AdditionalExamination);

router.post("/", AdditionalExaminationCrud.create);

router.get("/:id", AdditionalExaminationCrud.getOne);

router.get("/p/:id", AdditionalExaminationCrud.getByPatient);

router.delete("/:id", AdditionalExaminationCrud.delete);

router.get("/:id/:type", AdditionalExaminationCrud.getAllContent);

router.get("/:id/:type/:place", AdditionalExaminationCrud.getOneContent);

router.put("/:id/:type", AdditionalExaminationCrud.createContent);

router.put("/:id/:type/:place", AdditionalExaminationCrud.updateContent);

router.delete("/:id/:type/:place", AdditionalExaminationCrud.deleteContent);

module.exports = router;