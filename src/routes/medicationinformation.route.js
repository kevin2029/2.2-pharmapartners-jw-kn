const express = require("express");
const router = express.Router();

const MedicationInformation = require("../models/medicationinformation.model")();
const CrudController = require("../controllers/crud");
const MedicationInformationCrudController = new CrudController(
    MedicationInformation
);

router.post("/", MedicationInformationCrudController.create);

router.get("/", MedicationInformationCrudController.getAll);

router.get("/:id", MedicationInformationCrudController.getOne);

router.put("/:id", MedicationInformationCrudController.update);

router.delete("/:id", MedicationInformationCrudController.delete);

module.exports = router;