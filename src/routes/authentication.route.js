const express = require("express");
const router = express.Router();
const authcontroller = require("../controllers/authentication.controller");

// register
router.post("/register", authcontroller.register);

// login
router.post("/login", authcontroller.login);

//validate 2 factor authentication
router.post(
    "/validate/:id",
    authcontroller.validatecode,
    authcontroller.SignToken
);

module.exports = router;