const express = require("express");
const router = express.Router();

const Visitcard = require("../models/visitcard.model")();
const CrudController = require("../controllers/crud");
const VisitcardController = require("../controllers/visitcard.controller");
const VisitcardOneController = new VisitcardController(Visitcard);
const VisitcardCrudController = new CrudController(Visitcard);

const authenticationController = require("../controllers/authentication.controller");

router.post("/", VisitcardCrudController.create);

router.get(
    "/:id",
    authenticationController.validateToken,
    VisitcardOneController.getOne
);

router.get(
    "/p/:id",
    authenticationController.validateToken,
    VisitcardCrudController.getByPatient
);

router.delete("/:id", VisitcardCrudController.delete);

router.get(
    "/:id/:type",
    authenticationController.validateToken,
    VisitcardOneController.getAll
);

router.get(
    "/:id/:type/:place",
    authenticationController.validateToken,
    VisitcardOneController.getOneContent
);

router.put(
    "/:id/:type",
    authenticationController.validateToken,
    VisitcardCrudController.createContent
);

router.put("/:id/:type/:place", VisitcardCrudController.updateContent);

router.delete("/:id/:type/:place", VisitcardCrudController.deleteContent);

module.exports = router;