const express = require("express");
const router = express.Router();

const Visit = require("../models/visit.model")();
const CrudController = require("../controllers/crud");
const VisitController = require("../controllers/visit.controller");
const SpecialVisitController = new VisitController(Visit);
const VisitCrudController = new CrudController(Visit);

const authController = require("../controllers/authentication.controller");

router.post("/", VisitCrudController.create);

router.get("/", authController.validateToken, VisitCrudController.getAll);

router.get("/:id", VisitCrudController.getOne);

router.put("/:id", VisitCrudController.update);

router.delete("/:id", VisitCrudController.delete);

router.get(
    "/date/:date",
    authController.validateToken,
    SpecialVisitController.getByDate
);

module.exports = router;