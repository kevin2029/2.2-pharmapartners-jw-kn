const express = require("express");
const router = express.Router();

const Patient = require("../models/patient.model")();
const CrudController = require("../controllers/crud");
const PatientCrudController = new CrudController(Patient);

router.post("/", PatientCrudController.create);

router.get("/", PatientCrudController.getAll);

router.get("/:id", PatientCrudController.getOne);

router.put("/:id", PatientCrudController.update);

router.delete("/:id", PatientCrudController.delete);

module.exports = router;