const express = require("express");
const router = express.Router();

const PhysicalExamination = require("../models/physicalExamination.model")();
const CrudController = require("../controllers/crud");
const PhysicalExaminationCrud = new CrudController(PhysicalExamination);

router.post("/", PhysicalExaminationCrud.create);

router.get("/:id", PhysicalExaminationCrud.getOne);

router.get("/p/:id", PhysicalExaminationCrud.getByPatient);

router.delete("/:id", PhysicalExaminationCrud.delete);

router.get("/:id/:type", PhysicalExaminationCrud.getAllContent);

router.get("/:id/:type/:place", PhysicalExaminationCrud.getOneContent);

router.put("/:id/:type", PhysicalExaminationCrud.createContent);

router.put("/:id/:type/:place", PhysicalExaminationCrud.updateContent);

router.delete("/:id/:type/:place", PhysicalExaminationCrud.deleteContent);

module.exports = router;