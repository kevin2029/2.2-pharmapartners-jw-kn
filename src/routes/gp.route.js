const express = require("express");
const router = express.Router();

const GP = require("../models/gp.model")();
const CrudController = require("../controllers/crud");
const GPCrudController = new CrudController(GP);

router.post("/", GPCrudController.create);

router.get("/", GPCrudController.getAll);

router.get("/:id", GPCrudController.getOne);

router.put("/:id", GPCrudController.update);

router.delete("/:id", GPCrudController.delete);

module.exports = router;