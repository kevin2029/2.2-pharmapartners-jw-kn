const express = require("express");
const router = express.Router();

const LoggingModel = require("../models/logging.model")();
const CrudController = require("../controllers/crud");
const LogCrudController = new CrudController(LoggingModel);

router.get("/", LogCrudController.getLog);

module.exports = router;