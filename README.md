# PharmaPartners

Dit is de Gitlab voor het 2.2 project met PharmaPartners.<br />
Hierbij is er gebruik gemaakt van een Angular frontend en een Nodejs backend.<br />
Zie handleiding voor de installatie.<br />

De Url naar de Heroku Applicatie: [link](https://pharma-partners-app.herokuapp.com/)<br />

| Name | Student Number |
| ------ | ------ |
| Kevin Nguyen | 2150956 |
| Jan Willem Ruijzenaars | 2150617 |
